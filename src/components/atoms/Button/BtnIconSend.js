import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { ICSendDark, ICSendLight } from '../../../assets'
import { colors } from '../../../utils'

const BtnIconSend = ({disable, onPress}) => {
    if(disable){
        <View style={styles.container(disable)}>
            <ICSendDark />
        </View>
    }
    return (
        <TouchableOpacity style={styles.container(disable)} onPress={onPress}>
            <ICSendLight />
        </TouchableOpacity>
    )
}

export default BtnIconSend

const styles = StyleSheet.create({
    container: (disable) => ({
        backgroundColor: disable ? colors.disable : colors.wrapper,
        width: 45,
        height: 45,
        borderRadius: 10,
        paddingTop: 10,
        paddingRight: 3,
        paddingBottom: 8,
        paddingLeft: 8
    })
})
