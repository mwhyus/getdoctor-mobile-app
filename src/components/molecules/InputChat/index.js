import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import { colors, fonts } from '../../../utils'
import { Button } from '../../atoms'

const InputChat = ({value, onChangeText, onSend}) => {
    return (
        <View style={styles.container}>
            <TextInput
                placeholder="Type your messages..." 
                value={value} 
                onChangeText={onChangeText} 
                style={styles.input} 
            />
            <Button type='btn-icon-send' onPress={onSend} disable={value.length < 1} />
        </View>
    )
}

export default InputChat

const styles = StyleSheet.create({
    container: {
        padding: 16,
        flexDirection: 'row',
        backgroundColor: colors.white
    },
    input: {
        backgroundColor: colors.disable,
        padding: 14,
        borderRadius: 10,
        flex: 1,
        marginRight: 10,
        fontSize: 14,
        fontFamily: fonts.primary.normal,
        maxHeight: 45
    }
})
