import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import { DummyDoc1 } from '../../../assets'
import { colors, fonts } from '../../../utils'
import { Button } from '../../atoms'

const LightProfile = ({onPress, title, desc, photo}) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity>
                <Button type='icon-only' icon='back-dark' onPress={onPress} />
            </TouchableOpacity>
            <View style={styles.content}>
                <Text style={styles.name}>{title}</Text>
                <Text style={styles.desc}>{desc}</Text>
            </View>
            <Image source={photo} style={styles.avatar} />
        </View>
    )
}

export default LightProfile

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        paddingVertical: 22,
        paddingLeft: 20,
        paddingRight: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    content: {
        flex: 1
    },
    avatar: {
        width: 58,
        height: 58,
        borderRadius: 58 /2
    },
    name: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        textAlign: 'center'
    },
    desc: {
        fontSize: 16,
        fontFamily: fonts.primary.normal,
        marginTop: 6,
        textAlign: 'center',
        color: colors.text.secondary,
        fontFamily: fonts.primary[700]
    }
})
