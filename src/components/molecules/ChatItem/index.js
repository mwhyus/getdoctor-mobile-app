import React from 'react'
import IsDoctor from './IsDoctor'
import IsMe from './IsMe'

const ChatItem = ({isMe, text, date, photo}) => {
    if (isMe){
        return <IsMe text={text} date={date} />
    }
    return <IsDoctor text={text} date={date} photo={photo}/>
}

export default ChatItem