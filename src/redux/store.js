import { createStore } from "redux";

const initialState = {
    loading: false,
    name: 'Le Minarale',
    address: 'Konoha'

}

const reducer = (state = initialState, action) => {
    if(action.type === 'SET_LOADING'){
        return {
            ...state,
            loading: action.value
        }
    }
    
    return state
}

export const store = createStore(reducer)