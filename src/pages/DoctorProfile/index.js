import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { DummyDoc1 } from '../../assets'
import { Button, Gap, Header1, Profile, ProfileItem } from '../../components'
import { colors } from '../../utils'

const DoctorProfile = ({ navigation, route }) => {
    const dataDoctor = route.params
    return (
        <View style={styles.page}>
            <Header1 title='Doctor Profile' onPress={() => navigation.goBack()} />
            <Profile 
                name={dataDoctor.data.fullName} 
                desc={dataDoctor.data.profession} 
                photo={{uri: dataDoctor.data.photo}} 
            />
            <Gap height={10} />
            <ProfileItem label='Specialization' value={dataDoctor.data.profession} />
            <ProfileItem label='Location' value={dataDoctor.data.city} />
            <ProfileItem label='ID Number' value={dataDoctor.data.str_number} />
            <View style={styles.action}>
                <Button title='Start Conversation' onPress={() => navigation.navigate('Chatting', dataDoctor)} />
            </View>
        </View>
    )
}

export default DoctorProfile

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.white
    },
    action: {
        paddingHorizontal: 50,
        paddingTop: 54
    }
})
