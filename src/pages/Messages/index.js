import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { DummyDoc1 } from '../../assets'
import { ListDoctor } from '../../components'
import { Fire } from '../../config'
import { colors, fonts, getData } from '../../utils'

const Messages = ({navigation}) => {
    const [chatHistory, setChatHistory] = useState([])
    const [user, setUser] = useState({})
    
    useEffect(() => {
        getDataUserFromLocal()
        const rootDB = Fire.database().ref()
        const urlHistory = `messages/${user.uid}/`
        const messagesDB = rootDB.child(urlHistory)

        messagesDB.on('value', async snapshot => {
            if(snapshot.val()){
                const oldData = snapshot.val()
                const data = []
               const promises = await Object.keys(oldData).map(async key => {
                    const urlUidDoctor = `doctors/${oldData[key].uidPartner}`
                    const detailDoctor = await rootDB.child(urlUidDoctor).once('value')

                    console.log('detail dokter: ', detailDoctor.val());
                    data.push({
                        id: key,
                        detailDoctor: detailDoctor.val(),
                        ...oldData[key]
                    })
                })

                await Promise.all(promises)

                console.log('data hasil parse: ', data);
                setChatHistory(data)
            }
        })
    }, [user.uid])

    const getDataUserFromLocal = () => {
        getData('user').then(res => {
            // console.log("user login: ", res);
            setUser(res)
        })
    }

    return (
        <View style={styles.pages}>
            <View style={styles.content}>
                <Text style={styles.title}>Messages</Text>
            </View>
            <ScrollView>
                {
                    chatHistory.map(chat => {
                        const dataDoctor = {
                            // id: chat.detailDoctor.uid,
                            data: chat.detailDoctor
                        } 
                        return (
                            <ListDoctor
                                key={chat.id}
                                profile={{uri: chat.detailDoctor.photo}}
                                name={chat.detailDoctor.fullName}
                                chat={chat.lastContentChat}
                                onPress={() => navigation.navigate('Chatting', dataDoctor)}
                            />
                        )
                    })
                }
            </ScrollView>


        </View>
    )
}

export default Messages

const styles = StyleSheet.create({
    pages: {
        backgroundColor: colors.backgroundColor,
        flex: 1
    },
    content: {
        backgroundColor: colors.primary,
        height: 60,
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12,
        marginBottom: 20,

    },
    title: {
        alignSelf: 'center',
        alignItems: 'center',
        paddingTop: 14,
        fontSize: 22,
        fontFamily: fonts.primary[600]
    },
})
