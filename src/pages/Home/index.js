import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { ICMap, ILLDocDen, ILLDocGen, ILLDocPed, ILLDocPsy, ILLDocVet, ILLMedicine } from '../../assets'
import { DoctorCategory, Gap, HomeProfile, NewsItem } from '../../components'
import { Fire } from '../../config'
import { colors, fonts, getData, showError } from '../../utils'


const Home = ({ navigation }) => {
    const [news, setNews] = useState([])
    const [categoryDoctor, setCategoryDoctor] = useState([])

    useEffect(() => {
        Fire.database()
            .ref('news/')
            .once('value')
            .then(res => {
                // console.log('data news: ', res.val())
                if (res.val()) {
                    const data = res.val()
                    const filterData = data.filter(el => el != null)
                    // console.log("data hasil filter: ", filterData);
                    setNews(filterData)
                }
            })
            .catch(err => {
                showError(err.message)
            })

        Fire.database()
            .ref('category_doctor/')
            .once('value')
            .then(res => {
                // console.log('data category: ', res.val())
                if (res.val()) {
                    setCategoryDoctor(res.val())
                }
            })
            .catch(err => {
                showError(err.message)
            })

    }, [])

    useEffect(() => {
        getData('user').then(res => {
            // console.log('data user:', res)
        })
    }, [])

    // useEffect (() => {
    //     navigation.addListener('focus', () => 
    //         getData()
    //     )
    // })

    return (
        <ScrollView style={styles.page} showsVerticalScrollIndicator={false}>
            <View style={styles.content}>
                <View style={styles.smallcontainer}>
                    <View style={styles.header}>
                        {/* Komponen nya yg ini */}
                        <HomeProfile />
                        <TouchableOpacity onPress={() => navigation.navigate('Maps')}>
                            <ICMap />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.welcome}>How can we help you?</Text>
                    <View style={styles.category}>
                        <DoctorCategory
                            type1='General'
                            type2={categoryDoctor.categoryA}
                            pic={ILLDocGen}
                            onPress={() => navigation.navigate('ChooseDoctor')}
                        />
                        <Gap width={8} />
                        <DoctorCategory
                            type1=''
                            type2={categoryDoctor.categoryB}
                            pic={ILLDocPed}
                        />
                        <Gap width={8} />
                        <DoctorCategory
                            type1=''
                            type2={categoryDoctor.categoryC}
                            pic={ILLDocDen}
                        />
                    </View>
                    <Gap height={8} />
                    <View style={styles.category}>
                        <DoctorCategory
                            type2={categoryDoctor.categoryD}
                            pic={ILLDocPsy}
                            onPress={() => navigation.navigate('ChoosePsy')}
                        />
                        <Gap width={8} />
                        <DoctorCategory
                            type2={categoryDoctor.categoryE}
                            pic={ILLDocVet}
                        />
                        <Gap width={8} />
                        <DoctorCategory
                            type2={categoryDoctor.categoryF}
                            pic={ILLMedicine}
                        />
                    </View>
                </View>
                <View style={styles.article}>
                    <Text style={styles.sectionlabel}>Health Articles</Text>
                    <TouchableOpacity>
                        <Text style={styles.seeall}>See all</Text>
                    </TouchableOpacity>
                </View>
                {news.map(item => {
                    return (
                        <NewsItem
                            key={item.id}
                            title={item.title}
                            date={item.date}
                            image={item.image}
                        />
                    )
                })}
            </View>
        </ScrollView>
    )
}

export default Home

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.primary,
        flex: 1
    },
    smallcontainer: {
        paddingHorizontal: 16
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    content: {
        backgroundColor: colors.backgroundColor,
        flex: 1,
        paddingVertical: 20,
    },
    welcome: {
        fontSize: 21,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginBottom: 16
    },
    category: {
        flexDirection: 'row'
    },
    sectionlabel: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
    },
    article: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 20,
        marginBottom: 10,
        paddingHorizontal: 16
    },
    seeall: {
        color: colors.text.secondary,
        borderBottomWidth: 0.2
    }
})
