import React ,{useEffect} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ICLogo, ILFirst } from '../../assets'
import { Fire } from '../../config'

const Splash = ({navigation}) => {
  useEffect(() => {
    const unsubscribe = Fire.auth().onAuthStateChanged(user => {
      setTimeout(() => {
          if(user){
            //jika user nya login
            // console.log('user: ', user)
            navigation.replace('MainApp')
          } else {
            //user logout
            navigation.replace('GetStarted')
          }
      }, 2000)
    })

    return () => unsubscribe()
  },[navigation])
  
  return (
    <View style={styles.background}>
      <View style={styles.mainLogo}>
        <ICLogo />
      </View>

      <View style={styles.illustration}>
        <ILFirst />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mainLogo: {
    marginTop: 20,
    width: "100%",
    padding: 10
  },

  illustration: {
    alignItems: 'center',
    width: '100%',
    height: '90%',
    marginTop: '35%'
  },

  background: {
    backgroundColor: '#FFFFFF'
  }

})

export default Splash