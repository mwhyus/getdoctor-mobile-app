import React, { useEffect, useState } from 'react'
import { StyleSheet, View } from 'react-native'
import { Header1, ListDoctor } from '../../components'
import { Fire } from '../../config'
import { colors, showError } from '../../utils'

const ChoosePsy = ({navigation}) => {
    const [typeDoctor, settypeDoctor] = useState([]);

    useEffect(() => {
        Fire.database()
        .ref('doctors/')
        .orderByChild('category')
        .equalTo("Psychiatrist")
        .once('value')
        .then(res => {
            console.log('data list doctors: ', res.val())
            if (res.val()) {
                const oldData = res.val()
                const data = []
                Object.keys(oldData).map(item => {
                    data.push({
                        id: item,
                        data: oldData[item]
                    })
                })
                // console.log("parse nya: ", data);
                settypeDoctor(data)
            }
        })
        .catch(err => {
            showError(err.message)
        })


    }, [])

    return (
        <View style={styles.container}>
        <Header1 type='light' title= 'Select a Doctor' onPress={() => navigation.goBack()}/>
        {typeDoctor.map(doctor => {
            return (
                <ListDoctor 
                    key={doctor.id}
                    type='Chat' 
                    name={doctor.data.fullName} 
                    profile={{uri: doctor.data.photo}}
                    chat={doctor.data.city}
                    onPress={() => navigation.navigate('DoctorProfile', doctor)} 
                />
        )})}
    </View>
    )
}

export default ChoosePsy

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        flex: 1
    }
})
