import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { ChatItem, Header1, InputChat } from '../../components'
import { Fire } from '../../config'
import { colors, fonts, getChatTime, getData, setDateChat, showError } from '../../utils'


const Chatting = ({ navigation, route }) => {
    const dataDoctor = route.params
    const [chatContent, setChatContent] = useState("")
    const [user, setUser] = useState({})
    const [chatData, setChatData] = useState([])

    useEffect(() => {
        getDataUserFromLocal();
        const chatID = `${user.uid}_${dataDoctor.data.uid}`
        const urlFirebase = `chatting/${chatID}/allChat/`
        Fire
            .database()
            .ref(urlFirebase)
            .on('value', (snapshot) => {
                console.log('data chat: ', snapshot.val());
                if(snapshot.val()){
                    const dataSnapshot = snapshot.val()
                    const allDataChat = []

                    Object.keys(dataSnapshot).map(key => {
                        const dataChat = dataSnapshot[key]
                        const newDataChat = []

                        Object.keys(dataChat).map(itemChat => {
                            newDataChat.push({
                                id: itemChat,
                                data: dataChat[itemChat]
                            })
                        })
                        
                        allDataChat.push({
                            id : key,
                            data: newDataChat
                        })
                    })
                    console.log('all data chat: ', allDataChat);
                    setChatData(allDataChat)
                }
            })
    }, [dataDoctor.data.uid, user.uid])

    const getDataUserFromLocal = () => {
        getData('user').then(res => {
            // console.log("user login: ", res);
            setUser(res)
        })
    }
    
    const chatSend = () => {
        // console.log("Chat yang akan dikirimkan: ", chatContent);
        // console.log('usernya: ', user);
        const today = new Date()

        let chatID = `${user.uid}_${dataDoctor.data.uid}`
 
        const urlFirebase = `chatting/${chatID}/allChat/${setDateChat(today)}`
        const urlMessaagesUser = `messages/${user.uid}/${chatID}`
        const urlMessaagesDoctor = `messages/${dataDoctor.data.uid}/${chatID}`

        const data = {
            sendBy: user.uid,
            chatDate: today.getTime(),
            chatTime: getChatTime(today),
            chatContent: chatContent
        }

        const dataHistoryChatForUser = {
            lastContentChat: chatContent,
            lastChatDate: today.getTime(),
            uidPartner: dataDoctor.data.uid,
        }
        const dataHistoryChatForDoctor = {
            lastContentChat: chatContent,
            lastChatDate: today.getTime(),
            uidPartner: user.uid,
        }
        
        // console.log('data untuk dikirim: ', data);
        // console.log('Url Firebase: ', urlFirebase);
        //Kirim ke firebase
        Fire
            .database()
            .ref(urlFirebase)
            .push(data)
            .then(res => {
              setChatContent('')
              //Set history for user
              Fire
                .database()
                .ref(urlMessaagesUser)
                .set(dataHistoryChatForUser)

                //Set History for Doctor
                Fire.database()
                .ref(urlMessaagesDoctor)
                .set(dataHistoryChatForDoctor)
            //   console.log('yey berhasil', res);
            })
            .catch(err => {
                showError(err.message)
            })

    }

    return (
        <View style={styles.page}>
            <Header1 
                type='light-profile' 
                title={dataDoctor.data.fullName} 
                desc={dataDoctor.data.profession}
                photo={{uri: dataDoctor.data.photo}}
                onPress={() => navigation.goBack()} 
                
            />
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                {chatData.map(chat => {
                    return (
                        <View key={chat.id}>
                            <Text style={styles.chatDate}>{chat.id}</Text>
                            {chat.data.map(itemChat => {
                            const isMe = itemChat.data.sendBy == user.uid
                                return (
                                <ChatItem 
                                    key={itemChat.id}
                                    isMe={isMe} 
                                    text={itemChat.data.chatContent}
                                    date={itemChat.data.chatTime}
                                    photo={isMe ? null : {uri: dataDoctor.data.photo}}
                                />
                                )
                            })}


                            {/* <ChatItem isDoctor /> */}
                        </View>
                    )
                })}
                </ScrollView>
            </View>
            <InputChat
                value={chatContent}
                onChangeText={value => setChatContent(value)}
                onSend={chatSend}
            />

        </View>
    )
}

export default Chatting

const styles = StyleSheet.create({
    page: {flex: 1},
    chatDate: {
        fontSize: 11,
        fontFamily: fonts.primary.normal,
        color: colors.text.secondary,
        marginVertical: 20,
        textAlign: 'center'
    },
    content: {
        flex: 1
    }
})
