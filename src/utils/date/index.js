export const getChatTime = (date) => {
        const hour = date.getHours()
        const minutes = date.getMinutes()

        return (
            `${hour}:${minutes} ${hour >= 12 ? 'PM': 'AM'}`
        )
}

export const setDateChat = (oldDate) => {
    const year = oldDate.getFullYear()
    
    // +1 dikarenakan januari terhitung bulan ke 0 -
    // -sesuai flow JS yg menghitung dari 0
    const month = oldDate.getMonth() + 1
    const date = oldDate.getDate()

    return `${year}-${month}-${date}`
}