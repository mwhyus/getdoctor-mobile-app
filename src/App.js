import { NavigationContainer } from '@react-navigation/native';
import React, { useState } from 'react';
import { LogBox, YellowBox } from 'react-native';
import FlashMessage from "react-native-flash-message";
import { Provider, useSelector } from 'react-redux';
import { Loading } from './components';
import { store } from './redux/store';
import Router from './router';

const MainApp = () => {
  // const [loading, setLoading] = useState(false)
  const stateGlobal = useSelector(state => state) 
  // console.log('ini state global: ', stateGlobal)
  // console.disableYellowBox = true;
  // YellowBox.ignoreWarnings(["Setting a timer"])
  LogBox.ignoreAllLogs()
  return (
    <>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
      <FlashMessage position="top" />
      {stateGlobal.loading && <Loading />}
    </>
  )
}

const App = () => {
  return (
    <Provider store={store}>
      <MainApp />
    </Provider>
  )
}

export default App