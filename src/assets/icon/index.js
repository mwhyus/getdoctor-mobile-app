import ICLogo from './MainLogo.svg'
import ICBackDark from './arrow_back.svg'
import ICAddPhoto from './plus.svg'
import ICRemovePhoto from './remove.svg'
import ICHome from './Home.svg'
import ICMessages from './Messages.svg'
import ICSettings from './Settings.svg'
import ICHomeActive from './HomeActive.svg'
import ICMessagesActive from './MessagesActive.svg'
import ICSettingsActive from './SettingsActive.svg'
import ICMap from './map.svg'
import ICChatNow from './email.svg'
import ICSendLight from './send-light.svg'
import ICSendDark from './send-dark.svg'


export {
    ICLogo,
    ICBackDark,
    ICAddPhoto,
    ICRemovePhoto,
    ICHome,
    ICMessages,
    ICSettings,
    ICHomeActive,
    ICMessagesActive,
    ICSettingsActive,
    ICMap,
    ICChatNow,
    ICSendDark,
    ICSendLight
}